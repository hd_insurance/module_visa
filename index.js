import React, { useEffect, useState, createRef } from "react";
import TopBanner from "./top-banner.js";
import QnA from "./qna.js";
// import NeedSuport from "../../needsuport";
// import RegisterForm from "../../registerform";
import HorzLayout1 from "../../components/horzlayout/layout1";
import HorzLayout2 from "../../components/horzlayout/layout2";
// import ProductList from "../../products/list";
// import AppBanner from "../../appbanner";
import CardHolderInfo from "../../components/cardholderinfo";
import api from "../../services/Network";
import cogoToast from "cogo-toast";
import jwt from "jsonwebtoken";
import Layout from "../../components/layout.js";

const View = (props) => {
  const [cardOwner, setCardOwner] = useState({});
  const [activeTab, setActiveTab] = useState(0);
  const qnaRef = createRef();

  const searchInfor = async (data, webId) => {
    const res = await api.post(`/api/visa/search-gcn`, data);
    if (res) {
      const token = jwt.sign({ ...data, product: "ViSa" }, "doan-xem-la-gi");
      localStorage.setItem(`INIT_${webId}`, token);
      console.log(res)
      if (res?.data.length > 0) {
        setCardOwner(res?.data[0]);
        console.log("data", res.data[0]);
        return true;
      } else {
        setCardOwner({});
        cogoToast.error("Thông tin tra cứu không chính xác!");
        return false;
      }
    }
    return false;
  };
  const scrollToFAQ = (open_index) => {
    setActiveTab(open_index);
    window.scrollTo({
      top: qnaRef.current.offsetTop,
      left: 100,
      behavior: "smooth",
    });
  };

  return (
    <Layout>
      <TopBanner searchInfor={searchInfor} />
      <CardHolderInfo cardOwner={cardOwner} scrollToBT={scrollToFAQ} />
      <HorzLayout2 scrollToQL={scrollToFAQ}/>
      <HorzLayout1 />
      {/* <RegisterForm/>  */}
      {/* <ProductList/>  */}
      {/* <NeedSuport /> */}
      <QnA ref={qnaRef} activeTab={activeTab} />
      {/* <AppBanner /> */}
    </Layout>
  );
};

export default View;
