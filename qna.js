import React, { useEffect, useState, createRef, forwardRef } from "react";
import QnA from "../../components/qna";

const View = forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <QnA title={lng.get("igpheeaz")} idQna={"60fe77d1c3c0100fadb4bda6"} activeTab={props.activeTab} />
    </div>
  );
});

export default View;
