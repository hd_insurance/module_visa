import React, { useEffect, useState } from "react";
import TopBanner from "../../components/topbanner";
import Tilt from "react-tilt";
// import FLInput from "../../libs/flinput";
import Particles from "react-particles-js";
import { Button, Form } from "react-bootstrap";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile,
} from "react-device-detect";
// import { setCardOwner } from "../../../redux/actions/action-visa";
import Loading from "../../components/loading";
// import api from "../../../services/Network.js";
import cogoToast from "cogo-toast";
import jwt from "jsonwebtoken";
import globalStore from "../../services/globalStore";

import { FLInput } from "../../components/wrapuikit";

const params = {
  autoPlay: true,
  fullScreen: {
    enable: false,
    zIndex: -1,
  },
  fpsLimit: 60,
  particles: {
    bounce: {
      opacity: 1,
      horizontal: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
      vertical: {
        random: {
          enable: false,
          minimumValue: 0.1,
        },
        value: 1,
      },
    },

    color: {
      value: "#71A853",
      animation: {
        enable: false,
        speed: 200,
        sync: false,
      },
    },

    links: {
      blink: false,
      color: {
        value: "random",
      },
      consent: false,
      distance: 300,
      enable: true,
      frequency: 2,
      opacity: 1,
      triangles: {
        enable: false,
        frequency: 1,
      },
      width: 3,
      warp: true,
    },
    move: {
      angle: {
        offset: 45,
        value: 90,
      },
      attract: {
        enable: false,
        rotate: {
          x: 3000,
          y: 3000,
        },
      },
      direction: "none",
      distance: 0,
      enable: true,
      gravity: {
        acceleration: 9.81,
        enable: false,
        maxSpeed: 50,
      },

      random: false,
      size: false,
      speed: 2.5,
      straight: false,
      trail: {
        enable: false,
        length: 10,
        fillColor: {
          value: "#000000",
        },
      },
      vibrate: false,
      warp: false,
    },
    number: {
      limit: 0,
      value: isMobile ? 15 : 40,
    },
    opacity: {
      random: {
        enable: true,
        minimumValue: 0.1,
      },
      value: 0.8,
      animation: {
        destroy: "none",
        enable: true,
        minimumValue: 0.5,
        speed: 0.5,
        startValue: "random",
        sync: false,
      },
    },
    rotate: {
      random: {
        enable: false,
        minimumValue: 0,
      },
      value: 0,
      animation: {
        enable: false,
        speed: 0,
        sync: false,
      },
      direction: "clockwise",
      path: false,
    },
    shape: {
      options: {},
      type: "circle",
    },
    size: {
      random: {
        enable: true,
        minimumValue: 1,
      },
      value: 8,
      animation: {
        destroy: "none",
        enable: true,
        minimumValue: 1,
        speed: 2,
        startValue: "random",
        sync: false,
      },
    },
    stroke: {
      width: 0,
      color: {
        value: "#71A853",
        animation: {
          enable: false,
          speed: 0,
          sync: false,
        },
      },
      opacity: 0,
    },
  },
  pauseOnBlur: false,
  pauseOnOutsideViewport: false,
  themes: [],
};
const View = (props) => {
  const [isLoading, setLoading] = useState(false);
  const [validated, setValidated] = useState(false);
  const [ma, setMa] = useState("");
  const [emailPhone, setEmailPhone] = useState("");

  useEffect(() => {
    try {
      const webId = getWebId();
      const token = localStorage.getItem(`INIT_${webId}`);
      if (!token) return;
      const data = handleDecrypt(token);
      setMa(data.a2);
      setEmailPhone(data.a4);
      handleSearch(data);
    } catch (err) {
      console.log(err);
    }
  }, []);

  const handleDecrypt = (token) => {
    let data = jwt.verify(token, "doan-xem-la-gi");
    return data;
  };

  const getWebId = () => {
    const pageConfig = globalStore.get("pageconfig");
    const webId = pageConfig[0]?.WEB_ID;
    return webId;
  };

  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    if (form.checkValidity() === false) {
      setValidated(true);
    } else {
      setLoading(true);
      try {
        const data = {
          a1: lng.getLang() == "vi" ? "VN" : "EN",
          a2: ma.trim(),
          a4: emailPhone.trim(),
        };
        const webId = getWebId();
        const res = await props.searchInfor(data, webId);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    }
  };

  const handleSearch = async (obj) => {
    setLoading(true);
    try {
      const data = {
        a1: lng.getLang() == "vi" ? "VN" : "EN",
        a2: obj.a2,
        a4: obj.a4,
      };
      const webId = getWebId();
      const res = await props.searchInfor(data, webId);
      setLoading(false);
    } catch (error) {
      console.log(error);
      setLoading(false);
    }
  };

  return (
    <div>
      {isLoading ? <Loading isFullScreen={true} /> : null}
      <TopBanner>
        <Particles className={"cs_particles"} width={"100%"} height={"100%"} params={params} />

        <div
          className="topbanner-gb"
          style={{ backgroundImage: `url("/img/visa/bannertop.png")` }}
        >
          <div className="row">
            <div className="col-md-5 parent-class mobile">
              <div className="divpcard">
                <img className="divcard" src={"/img/visa/card.png"} />
              </div>
            </div>

            <div className="col-md-7">
              <div className="clf-banner">
                <h3 lng="0ecdhgrm">{lng.get("0ecdhgrm")} </h3>
                <h1 lng="tibpt6yf">{lng.get("tibpt6yf")}</h1>
                <p lng="xfzregcg">{lng.get("xfzregcg")}</p>
                <div className="form-search-visa">
                  <div className="input-mtc" lng="oyzm5cae">
                    {lng.get("oyzm5cae")}
                  </div>
                  <Form
                    className="f-orm"
                    onSubmit={handleSubmit}
                    noValidate
                    validated={validated}
                  >
                    <div className="inner-form-search-visa">
                      <div className="input-hor-group">
                        <FLInput
                          label={lng.get("pikeqyg8")}
                          hideborder={true}
                          value={ma}
                          required={true}
                          changeEvent={setMa}
                        />
                        <div className="hdrive" />
                        <FLInput
                          label={lng.get("8sbivizu")}
                          hideborder={true}
                          value={emailPhone}
                          required={true}
                          changeEvent={setEmailPhone}
                        />
                      </div>

                      <Button type="submit" className="check" lng="sqcgvcsn">
                        {lng.get("sqcgvcsn")}
                      </Button>
                    </div>
                  </Form>
                </div>
              </div>
            </div>

            <div className="col-md-5 parent-class desktop">
              <Tilt
                className="Tilt"
                options={{ reverse: true, max: 25, scale: 1.1 }}
              >
                <div className="divpcard">
                  <img className="divcard" src={"/img/visa/card.png"} />
                </div>
              </Tilt>
            </div>
          </div>
        </div>
      </TopBanner>
    </div>
  );
};

export default View;
